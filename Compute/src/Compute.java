import java.util.Scanner;
public class Compute {

	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		float x = input.nextFloat();
		float y = (float) 1 / x;
		System.out.print("x = " + x + "\ny = " + y + "\nx * y = " + (x * y) + "\n(x * y) - 1 = " + ((x * y) - 1));
		input.close();
		//x * y always equals 1, (x * y) - 1 always equals 0
	}

}
